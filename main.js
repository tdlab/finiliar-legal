(function () {

  window.removeFiniliarDefModal = (_) => {
    $(".finiliar-defModal").remove()
  }

  var leftContent = document.getElementById("content-left");
  var rightContent = document.getElementById("content-right");

  rightContent.innerHTML = '';
  leftContent.innerHTML += '<h4>Please carefully read this important document, and store it in a safe place. These Financial Services Terms (Terms) are effective January 13, 2020 and replace all previous versions.</h4> \n';
  leftContent.innerHTML += '<div class="text-md-left">These Terms apply to your Account and any Services you use. \
    These Terms include the <i> About Our Interest Calculations and About Our Accounts And Related Services </i> documents. You should read them together along with any other agreements that apply to your Account or Services. </div> <hr />';
  leftContent.innerHTML += '<h3>Table Of Contents</h3> \
    <h4> Section 1: Definitions </h4>\
    <h4> Section 2: Account Fees, Activity, And Records </h4> \
    2.1 &nbsp; What fees, charges, interest, and costs apply to your Account and Services? <br>\
    2.2 &nbsp; How can you use your Account? <br>\
    2.3 &nbsp; Can we restrict your Account activity or close your Account? <br>\
    2.4 &nbsp; How do you prove legal capacity? <br>\
    2.5 &nbsp; Can your Legal Representative access your Account? <br>\
    2.6 &nbsp; How do we handle a Payment Instrument? <br>\
    2.7 &nbsp; What Records are available, and what is your responsibility for verifying them? <br>\
    2.8 &nbsp; What interest rates apply to your Account? <br>\
    2.9 &nbsp; How do we manage an overdraft not covered by an overdraft protection service? <br>\
    2.10 How do we handle foreign currency? <br>\
    <h4> Section 3: Joint Accounts </h4> \
    3.1 &nbsp; How do we operate joint Accounts? <br>\
    3.2 &nbsp; Can we restrict access to or close your joint Account? <br>\
    <h4> Section 4: Communications </h4> <br>\
    4.1 &nbsp; How do we manage electronic or other communications? <br>\
    4.2 &nbsp; How do we manage Electronic Signatures and instructions? <br>\
    <h4> Section 5: Account Changes </h4> \
    5.1 &nbsp; How do we make changes to the terms and conditions of your Account and Services? <br>\
    5.2 &nbsp; Can we move your Account to another branch? <br>\
    5.3 &nbsp; Can we discontinue offering an Account or Service? <br>\
    5.4 &nbsp;  What happens to funds in your Account upon your death? <br>\
    5.5 &nbsp; Can we transfer, sell, or assign your Account or Services? <br>\
    <h4> Section 6: General Information </h4>  \
    6.1 &nbsp;  What is our liability for any loss or damages? <br>\
    6.2 &nbsp; Can we set-off debts you owe to any TD Bank Group member? <br> \
    6.3 &nbsp; How do we notify you of a legal process or claim? <br>\
    6.4 &nbsp; How do we manage court and other legal or administrative demands on your Account? <br> \
    6.5 &nbsp; What if we do not act immediately if you breach these Terms? <br>\
    6.6 &nbsp; Who is bound by these Terms? <br> \
    <h4> Section 7: How To Contact Us </h4>\
    7.1  &nbsp; How do you contact us with complaints? <br>\
    7.2  &nbsp; How do you contact us with general inquiries? <br>\
    <h4> Section 8: How To Contact The Financial Consumer Agency Of Canada (FCAC) </h4> <hr />\
    ';

  rightContent.innerHTML += '<h4> Section 1: Definitions </h4>\
    <div><b><i>Account</i></b> means any personal deposit account opened with TD. It does not include guaranteed investment certificates (GICs), term deposits, or registered plans, except for the High Interest TFSA Savings Account. To determine which TD entity issued your Account, please refer to the <i>About Our Accounts And Related Services</i> document. You can find this document at any of our branches and online at td.com </div>\
    <div><b><i>Legal Representative</i></b> means: <br>\
    · Any person named as such in a Power of Attorney for property \
    and, in Quebec, any person named as a mandatary in a mandate <br>\
    · A committee of estate <br>\
    · An estate representative <br>\
    · A legal guardian and, in Quebec, a curator <br>\
    · Any other person recognized as your legal representative under applicable law or by a court order </div> \
    <div><b><i>Service</i></b> means any service we make available to you in connection with your Account as set out in the <i> About Our Accounts And Related Services </i> document.</div> \
    <div><b><i>We, us, our, TD Bank Group, </i></b> or <b><i>TD</i></b> refers to The Toronto- Dominion Bank and its affiliates, including TD Mortgage Corporation, TD Pacific Mortgage Corporation, and The Canada Trust Company.</div> \
    <div><b><i>You, your,</i></b>  or <i><b> yours</i></b> means the customer or customers listed on the Account and their Legal Representative, if applicable.</div> \
    <hr />';

  rightContent.innerHTML += '<h4>Section 2: Account Fees, Activity, And Records</h4> \
    <h4>2.1 &nbsp; What fees, charges, interest, and costs apply to your Account and Services? </h4> \
    <p>You agree to pay and authorize us to charge to your Account - even if it creates or increases an overdraft - any fees, charges, interest costs, or other amounts that: <br> \
    · You owe TD for your Account and the Services that you use, as set out in the <i> About Our Accounts And Related Services </i> document. <br> \
    · You owe to another financial institution as a result of using your Account or a Service. <br> \
    · We incur when: <br> \
    &nbsp;&nbsp;&nbsp;&nbsp;· We attempt to recover any amount that you owe us and \
     any related legal costs. <br>\
    &nbsp;&nbsp;&nbsp;&nbsp;· We enforce any obligation that you owe us. <br>\
    &nbsp;&nbsp;&nbsp;&nbsp;· We respond to any legal or administrative proceedings, notices, or demands related &nbsp;&nbsp;&nbsp;&nbsp;to your Account or any Services that you used. <br> \
    <p> Our fees, charges, and interest are subject to change from time to time. You can find TD\'s list of Account fees and charges, as well as features of any Account or Services, in the <i> About Our Accounts And Related Services </i> document. You can access this document at any of our branches or online at td.com </p> \
    <h4>2.2 &nbsp; How can you use your Account?</h4> \
    <p>Your Account is for personal, household, or family use only. <br>\
    You cannot use your Account to: <br> \
    · Manage a business or other enterprise. <br>\
    · Conduct or support any fraudulent, illegal, or improper activities. <br>\
    · Engage in activities that we believe expose us to unacceptable risk. <br>\
    If you do so, we have the right to restrict or close your Account with or without notifying you. <br>\
    <b>Accessing Your Account</b> \
    You can access your Account and make transactions in a variety of ways. The use of your Access Card and electronic financial services are subject to these Terms and our <i> Cardholder And Electronic Financial Services Terms And Conditions. </i>  \
    Some of the ways you can use your Access Card and electronic financial services include: <br>\
    · Automated Banking Machine ("ATM") · EasyLine® telephone banking <br>\
    · EasyWeb® online banking <br>\
    · Merchants (point-of-sale terminal) <br>\
    · Mobile banking <br></p> \
    ';

  var lastAction = 0;
  var keywords = [
    {
      word: "Account",
      classId: "account",
      definition:
        "Any personal deposit account opened with TD. It does not include guaranteed investment certificates (GICs), term deposits, or registered plans, except for the High Interest TFSA Savings Account. To determine which TD entity issued your Account, please refer to the About Our Accounts And Related Services document. ",
    },
    {
      word: "Legal Representative",
      classId: "legal_representative",
      definition: "· Any person named as such in a Power of Attorney for property and,  in Quebec, any person named as a mandatary in a mandate  <br> \· A committee of estate  <br> \· An estate representative  <br> \· A legal guardian and, in Quebec, a curator <br> \· Any other person recognized as your legal representative under applicable law or by a court order <br>"
    },
    {
      word: "Service",
      classId: "service",
      definition: "any service we make available to you in connection with your Account as set out in the About Our Accounts And Related Services document."

    },
    {
      word: "We",
      classId: "we",
      definition: "The Toronto- Dominion Bank and its affiliates, including TD Mortgage Corporation, TD Pacific Mortgage Corporation, and The Canada Trust Company."
    },
    {
      word: "Us",
      classId: "us",
      definition: "The Toronto- Dominion Bank and its affiliates, including TD Mortgage Corporation, TD Pacific Mortgage Corporation, and The Canada Trust Company."
    },
    {
      word: "Our",
      classId: "our",
      definition: "The Toronto- Dominion Bank and its affiliates, including TD Mortgage Corporation, TD Pacific Mortgage Corporation, and The Canada Trust Company."
    },
    {
      word: "TD Bank Group",
      classId: "td_bank_group",
      definition: "The Toronto- Dominion Bank and its affiliates, including TD Mortgage Corporation, TD Pacific Mortgage Corporation, and The Canada Trust Company."
    },
    {
      word: "TD",
      classId: "td",
      definition: "The Toronto- Dominion Bank and its affiliates, including TD Mortgage Corporation, TD Pacific Mortgage Corporation, and The Canada Trust Company."
    },
  ];

  const drawhighLighhts = () => {
    for (let i = 0; i < keywords.length; i++) {
      $("p").mark(keywords[i].word, {
        separateWordSearch: false,
        accuracy: "exactly",
        // done: function(count){
        //   totalCount +=count;

        // },
        each: function (node) {
          node.addEventListener(
            "mouseenter", function (event) {
              let now = (new Date().getTime());
              lastAction = now;
              setTimeout(function () {
                var bodyElement = $("body");
                var defModal = "";
                // Container
                defModal += "<div ";
                defModal += `class='finiliar-defModal finiliar-${keywords[i].classId}'`;
                defModal += `style='
                                background: white !important;
                                position: fixed !important;
                                top: ${event.clientY}px !important;
                                left: ${event.clientX}px !important;
                                width: 170px !important;
                                margin: 5px !important;
                                min-height: 150px !important;
                                overflow-y: scroll !important;
                                z-index: 99999999999 !important;
                                display: inline-block !important;
                                border-radius: 5px !important;
                                box-shadow: 0 0.5px 8px 0 rgba(0,0,0,0.19) !important;
                                padding: 10px !important;'
                              >`;
                // close button
                defModal += `<button
                                class="finiliar-closeButton"
                                style="
                                    position: absolute;
                                    top: 10px;
                                    right: 10px;
                                    height: 15px;
                                    width: 15px;
                                    padding: 0px;
                                    line-height: 0px;
                                    outline: none;
                                    border: none;
                                    color: white;
                                    background: black;
                                    border-radius: 7.5px;
                                "
                                onclick="window.removeFiniliarDefModal()"
                                aria-label="Close definition popup"
                              >
                                <span style="margin-top: -2px; display: block">×</span>
                              </button>
                `
                // Title
                defModal += "<p ";
                defModal += `style='
                                font-weight: 700 !important;
                                font-size: 13px !important;
                                line-height: 15px !important;
                                margin: 0px !important;
                                letter-spacing: 0.1px;
                                color: #313030;
                                width: 75%;
                                font-family: "Verdana", "Geneva", "sans-serif";'
                              >`;
                defModal += `${keywords[i].word}`;
                defModal += "</p>";
                // Defination
                defModal += "<p ";
                defModal += `style='font-weight: normal !important;
                                 padding-top: 5px !important;
                                 font-size: 12px !important;
                                 color: #464545 !important;
                                 font-family: "Verdana", "Geneva", "sans-serif" !important;'
                                >`;
                defModal += `${keywords[i].definition}`;
                defModal += "</p>";

                defModal += "</div>";
                if (lastAction === now)
                  bodyElement.prepend(defModal);

              }, 150);
            }, false);

          node.addEventListener("mouseleave", function (event) {
            let now = (new Date().getTime());
            lastAction = now;
            setTimeout(function () {
              if (lastAction === now) {
                var bodyElement = $(`#finiliar-${keywords[i].classId}`);
                bodyElement.fadeOut('fast');
              }
            }, 150);
          }, false);
        }
      });
    }


    $("mark").css({
      background: "transparent",
      "border-bottom": "1px #228B22 solid",
      "padding-bottom": "0.5px !important",
      "display": "iniine-block"
    });

  }
  drawhighLighhts();

})();
